import LoginPage from './components/login/LoginPage'
import HomePage from './components/Home/HomePage'
import Grid from '@material-ui/core/Grid'
import './App.css'

function App() {
  return (
      <div className="App">
          {
              localStorage.getItem("user") == undefined || localStorage.getItem("user") == null ?
              <LoginPage />:<HomePage />
          }
    </div>
  );
}

export default App;
