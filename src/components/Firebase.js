import firebase from 'firebase';

require('firebase/auth')

firebase.initializeApp({
    apiKey: "AIzaSyAHkvO0nGeY19LdJqPHxDmrjZ_PIPajtcc",
    authDomain: "chatze-bad10.firebaseapp.com",
    projectId: "chatze-bad10",
    storageBucket: "chatze-bad10.appspot.com",
    messagingSenderId: "604380134045",
    appId: "1:604380134045:web:d618ac08657b83e25a3274",
    measurementId: "G-KCELFJZ8C1"
});

const auth = firebase.auth();
const storage = firebase.storage();

export { auth, storage };