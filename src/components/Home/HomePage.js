import React, { Component } from 'react';
import Navbar from '../Navbar/Navbar';
import MainContent from '../MainContent/MainContent';

class HomePage extends Component {

    constructor(props) {
        super();

    }

    render() {
        return (
            <div>
                <Navbar />
                <MainContent/>
            </div>
        );
    }

}

export default HomePage;