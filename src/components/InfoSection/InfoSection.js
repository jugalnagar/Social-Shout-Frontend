import React, { Component } from 'react';
import './InfoSection.css';
import pp1 from '../../images/pp1.png';
import Avatar from '@material-ui/core/Avatar'



class InfoSection extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user: JSON.parse(localStorage.getItem("user")),
        }
        console.log(this.state.user);
    }

    
    
    render() {
        return (

            <div className="infosection">
                <Avatar src={pp1} className="info_image" />
                <div className="info_userinfo">
                    <div className="info_username">
                        {this.state.user.userName}

                    </div>
                    <div className="info_description">
                        Discription
                    </div>
                </div>
            </div>

        );
    }
}

export default InfoSection;