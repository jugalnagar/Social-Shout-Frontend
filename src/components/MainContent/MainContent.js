import React, { Component } from 'react';
import './MainContent.css';
import { Grid } from '@material-ui/core';

import find_icon from '../../images/find.svg';
import pp1 from '../../images/pp1.png';
import Avatar from '@material-ui/core/Avatar';
import StatusBar from '../StatusBar/StatusBar';
import PostBar from '../PostBar/PostBar';
import InfoSection from '../InfoSection/InfoSection';
import SuggestionSection from '../SuggestionSection/SuggestionSection';

class MainContent extends Component {

    constructor(props) {
        super(props);

    }

    render() {
        return (

            <div className="maincontent">
                <Grid container>
                    <Grid item xs={3}></Grid>
                    <Grid item xs={5}>
                        <div>
                            <StatusBar />
                            <PostBar/>
                        </div>
                    </Grid>
                    <Grid item xs={2}>
                        <InfoSection />
                        <SuggestionSection/>
                    </Grid>
                    <Grid item xs={1}></Grid>
                   
                </Grid>
            </div>

        );
    }
}

export default MainContent;