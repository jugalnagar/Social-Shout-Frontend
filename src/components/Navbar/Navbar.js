import React, { Component } from 'react';
import './Navbar.css';
import { Grid } from '@material-ui/core';
import insta_logo from '../../images/logo.png';
import home_icon from '../../images/home.svg';
import love_icon from '../../images/love.svg';
import message_icon from '../../images/message.svg';
import find_icon from '../../images/find.svg';
import pp1 from '../../images/pp1.png';
import Avatar from '@material-ui/core/Avatar';

class Navbar extends Component {

    constructor(props) {
        super(props);
        
    }

    render() {
        return (

            <div className="navbar">
                <Grid container>
                    <Grid item xs={3}></Grid>
                    <Grid item xs={6}>
                        <div className="navbar_component">
                            <div className="navbar_logo">
                                <img src={insta_logo} className = "logo"/>
                            </div>
                            <div className="navbar_search">
                                <input type="text" placeholder="Search" className="searchbar"/>
                            </div>
                            <div className="navbar_icon">
                                <img src={home_icon} style={{"width":"40px","height":"40px"}}/>
                                <img src={love_icon} style={{ "width": "40px", "height": "40px" }} />
                                <img src={find_icon} style={{ "width": "40px", "height": "40px" }} />
                                <img src={message_icon} style={{ "width": "40px", "height": "40px" }} />
                                <Avatar src={pp1} style={{ "maxWidth": "25px", "maxHeight": "25px" }}/>
                            </div>

                        </div>
                    </Grid>
                    <Grid item xs={3}></Grid>
                </Grid>
            </div>
            
        );
    }
}

export default Navbar;