import React, { Component } from 'react';
import './Post.css';
import { Grid, Avatar } from '@material-ui/core';
import pp1 from '../../images/pp1.png';
import post from '../../images/post.jpg';
import like from '../../images/love.svg';
import comment from '../../images/comment.svg';
import share from '../../images/share.svg';

class Post extends Component {

    constructor(props) {
        super(props);
        this.state = {
            commentList: []
        }


    }

    componentDidMount() {
        this.getCommentData();
    }

    getCommentData() {
        let data = [
            { "comment":"nice pic","username":"im_rahulnagar" },
            { "comment": "wa bete moj krdi!!!", "username": "jugal_nagar" },
            { "comment": "American accent", "username": "harsh_rathod" },
            { "comment": "Hmm....", "username": "lakhan_barfa" },
        ]

        this.setState({ commentList:data})
    }

    render() {
        return (

            <div className="post_container">
                <div className="post_header">
                    <Avatar className="post_profile" src={this.props.profile} />
                    <div className="post_username">{this.props.username}</div>
                </div>
                <div>
                    <img src={this.props.image} width="615px"/>
                </div>
                <div>
                    <div>
                        <img src={like} className="post_analyticsicon" />
                        <img src={comment} className="post_analyticsicon"/>
                        <img src={share} className="post_analyticsicon"/>
                    </div>
                    <div style={{"fontWeight":"bold","marginLeft":"10px"}}>
                        {this.props.like} like
                    </div>
                </div>
                <div>
                    {
                        this.state.commentList.map((item,index) => {
                            return (
                                <div className="post_comment">
                                    <div style={{ "fontWeight":"bold","marginRight":"5px" }}>
                                        {item.username}
                                    </div>
                                    {item.comment}
                                </div>  
                            );
                        })
                    }
                 
                    <input type="text" placeholder="Add Comment..." className="post_commentinput"/>
                </div>
            </div>

        );
    }
}

export default Post;