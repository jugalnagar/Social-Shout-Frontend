import React, { Component } from 'react';
import './PostBar.css';
import { Grid } from '@material-ui/core';
import Post from '../Post/Post';
import upload from '../../images/upload.png';
import { auth, storage } from '../Firebase'
import ProgressBar from 'react-bootstrap/ProgressBar'

class PostBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            progressBar:0,
            postFile: null,
            postDiscription:null,
            postList: []
        }
    }

    componentDidMount() {
        this.getPostList();
    }

    getPostList() {
        let data = [
            {
                "username": "rahul_rajput",
                "image":"https://static.zerochan.net/Cross-Over.full.1255890.jpg",
                "like":"110"
            },
            {
                "username": "rahul_rajput",
                "image": "https://static.zerochan.net/Cross-Over.full.1255890.jpg",
                "like": "110"
            },
            {
                "username": "rahul_rajput",
                "image": "https://static.zerochan.net/Cross-Over.full.1255890.jpg",
                "like": "110"
            },
            {
                "username": "rahul_rajput",
                "image": "https://static.zerochan.net/Cross-Over.full.1255890.jpg",
                "like": "110"
            },
            {
                "username": "rahul_rajput",
                "image": "https://static.zerochan.net/Cross-Over.full.1255890.jpg",
                "like": "110"
            },
            {
                "username": "rahul_rajput",
                "image": "https://static.zerochan.net/Cross-Over.full.1255890.jpg",
                "like": "110"
            },
            {
                "username": "rahul_rajput",
                "image": "https://static.zerochan.net/Cross-Over.full.1255890.jpg",
                "like": "110"
            },
        ]

        this.setState({ postList: data });
    }

    upload = () => {
        const thisContext = this;
        if (this.state.postFile == null || this.state.postFile == undefined) {
            return;
        }

        var uploadTask = storage.ref("images").child(this.state.postFile.name).put(this.state.postFile);
        uploadTask.on(
            "state_changed",
            function (snapshot) {
                var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                thisContext.setState({ progressBar: progress });
            },
            function (error) {
            },
            function () {
                uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
                    console.log(downloadURL);

                    let payload = {
                        "description": thisContext.postDiscription,
                        "postPic": downloadURL
                    }

                    const requestOptions = {
                        method: "POST",
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify(payload),
                    }

                    fetch("http://localhost:8889/post/"+JSON.parse(localStorage.getItem("user")).mobile, requestOptions)
                        .then(response => response.json())
                        .then(data => {
                            console.log(data);
                            //thisContext.getPost();
                        })
                        .catch(error => {

                        })

                })
            }
        );


    }

    render() {
        return (

            <div className="">
                <div className="post_upload">
                    <div>
                        <label for="post">
                        <img src={upload} className="upload_icon" />
                        </label>
                        <input type="file" id="post" className="post_input" onChange={e => this.setState({ postFile: e.target.files[0] })} />
                    </div>
                    <input type="text" className="upload_statement" placeholder="Upload New Post" onChange={e => this.setState({ postDiscription: e.target.value })} />
                    <button className="post_button" disabled={!this.state.postFile} onClick={this.upload} >Post</button>
                </div>
                {this.state.postFile ? <ProgressBar animated variant="success" now={this.state.progressBar} /> : <div></div> }
                {
                    this.state.postList.map((item, index) => {
                        return (
                            <Post username={item.username} image={item.image} like={ item.like} />
                        );
                    })
                }

                
                
                
            </div>

        );
    }
}

export default PostBar;