import React, { Component } from 'react';
import '../login/LoginPage.css';
import { storage, auth } from '../Firebase.js';

class SignIN extends Component {

    constructor(props) {
        super(props)
        this.state = {
            email: null,
            password:null
        }
    }

    login = () => {
        auth.signInWithEmailAndPassword(this.state.email, this.state.password)
            .then((userCredential) => {
                
                let payload = {
                    "email": this.state.email,
                    "password": this.state.password
                   
                }


                const requestOptions = {
                    method: "GET",
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(payload),
                }

                fetch('http://localhost:8889/user/login?email='+this.state.email+'&password='+this.state.password)
                    .then(response => response.json())
                    .then(data => {
                        var user = data;
                        localStorage.setItem("user", JSON.stringify(user));
                        localStorage.setItem("username", user.username);
                        window.location.reload();
                        console.log("user "+user.userName);
                        console.log("data ",data)
                    })
                    .catch(error => {
                        console.log("error idii"+error);

                    })

                console.log("hello");

                
            })
            .catch((error) => {
                var errorCode = error.code;
                var errorMessage = error.message;
            });
    }

    render() {
        return (
            <div className="loginpage_signin">
                <input className="loginpage_text" type="text" onChange={(event) => { this.state.email = event.currentTarget.value; }} placeholder="Email" />
                <input className="loginpage_text" type="password" onChange={(event) => { this.state.password = event.currentTarget.value; }} placeholder="Password" />
                <button className="loginpage_login" onClick={this.login}>Log In</button>
            </div>

        );
    }
}

export default SignIN;