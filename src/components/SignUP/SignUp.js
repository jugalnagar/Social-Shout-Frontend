import React, { Component } from 'react';
import '../login/LoginPage.css';
import { storage, auth } from '../Firebase.js';

class SignUp extends Component {

    constructor(props) {
        super(props)
        this.state = {
            mobile:null,
            fullName:null,
            username:null,
            email:null,
            password:null
        }
    }

    addUser = ()=>{

        auth.createUserWithEmailAndPassword(this.state.email, this.state.password)
            .then((userCredential) => {
                
                var user = userCredential.user;

                let payload = {
                    "email": this.state.email,
                    "fullName": this.state.fullName,
                    "mobile": this.state.mobile,
                    "password": this.state.password,
                    "userName": this.state.username
                }
                

                const requestOptions = {
                    method: "POST",
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(payload),
                }

                fetch("http://localhost:8889/user/signup", requestOptions)
                    .then(response => response.json())
                    .then(data => {
                        localStorage.setItem("users", JSON.stringify(user));
                        window.location.reload();
                    })
                    .catch(error => {
                        console.log(error);

                    })
                
            })
            .catch((error) => {
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log(errorMessage);
              
            });
    }

    render() {
        return (
            <div className="loginpage_signin">
                <input className="loginpage_text" type="text" onChange={(event) => { this.state.mobile = event.currentTarget.value;}} placeholder="Mobile Number" />
                <input className="loginpage_text" type="text" onChange={(event) => { this.state.fullName = event.currentTarget.value; }} placeholder="Full Name" />
                <input className="loginpage_text" type="text" onChange={(event) => { this.state.username = event.currentTarget.value; }} placeholder="Username" />
                <input className="loginpage_text" type="text" onChange={(event) => { this.state.email = event.currentTarget.value; }} placeholder="Email" />
                <input className="loginpage_text" type="password" onChange={(event) => { this.state.password = event.currentTarget.value; }} placeholder="Password" />
                <button className="loginpage_login" onClick={this.addUser}>Sign Up</button>
            </div>

        );
    }
}

export default SignUp;