import React, { Component } from 'react';
import './StatusBar.css';
import { Grid } from '@material-ui/core';
import upload from '../../images/upload1.png';
import Avatar from '@material-ui/core/Avatar';


class StatusBar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            statusList: []
        }
    }

    componentDidMount() {
        this.getData();
    }

    getData = () => {
        let data = [
            {
                "username": "jugal_nagar",
                "imageURL": "../../images/pp1.png"
            },
            {
                "username": "jugal_nagar",
                "imageURL": "../../images/pp1.png"
            },
            {
                "username": "jugal_nagar",
                "imageURL": "../../images/pp1.png"
            },
            {
                "username": "jugal_nagar",
                "imageURL": "../../images/pp1.png"
            },
            {
                "username": "jugal_nagar",
                "imageURL": "../../images/pp1.png"
            },
            {
                "username": "jugal_nagar",
                "imageURL": "../../images/pp1.png"
            }
        ]

        this.setState({ statusList: data });
    }


    render() {
        return (

            <div className="">

                <div className="statusbar">

                    <div className="story_upload">
                       <img src={upload} className="story_icon" />
                  
                    </div>

                    {
                        this.state.statusList.map((item, index)=>{
                            return (
                                <div className="status">
                                    <Avatar className="statusbar_status" src={item.imageURL} />
                                    <div className="statusbar_profile">
                                        {item.username}
                                    </div>
                                </div>
                            );
                        })
                    }
                 </div>
            </div>

        );
    }
}

export default StatusBar;