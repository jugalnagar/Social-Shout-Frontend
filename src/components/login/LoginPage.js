import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid'
import mobile from '../../images/9364675fb26a.svg'
import insta_logo from '../../images/logo.png'
import fb_logo from '../../images/fb.png';
import './LoginPage.css';
import SignIN from '../SignIN/SignIN';
import SignUp from '../SignUP/SignUp';

class LoginPage extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isLogin: true
        }
    }

    onChange = () => {
        if (this.state.isLogin)
            this.setState({ isLogin: false })
        else
            this.setState({isLogin: true})
    }

    render() {
        return (
            <div className="login_page">
                <Grid container>
                    <Grid item xs={3}></Grid>
                    <Grid item xs={6}>
                        <div className="login_content">
                            <div className="login_mobile">
                                <img src={mobile} width="454px" />
                            </div>

                            <div className="loginpage_rightside">

                                <div className="loginpage_component">
                                    <div>
                                        <img src={insta_logo} className="loginpage_logo" />
                                    </div>

                                    {
                                        this.state.isLogin ? <SignIN/> : <SignUp/>

                                    }
                                    
                                    <div className="login_divd">
                                        <div className="loginpage_divder"></div>
                                        <div className="loginpage_divdercontent">OR</div>
                                        <div className="loginpage_divder"></div>
                                    </div>

                                    <div className="loginpage_fb">
                                        <img src={fb_logo} className="loginpage_facebook"/>Log in with Facebook
                                    </div>

                                    <div className="loginpage_forget">
                                        Forget Password ??????????
                                    </div>
                               
                                </div>

                                <div className="loginpage_signupoption">
                                    {
                                        this.state.isLogin ?
                                            <div className="loginpage_signup">Don't have an account? <span onClick={this.onChange} style={{ "font-weight": "bold", "color": "#0395F6" }}>Sign up</span></div>
                                            : <div className="loginpage_signin">Have an account? <span onClick={this.onChange} style={{ "font-weight": "bold", "color": "#0395F6" }}>Sign in</span></div>
                                    } 
                                </div>
                            </div>
                           
                        </div>
                        
                    </Grid>
                    <Grid item xs={3}></Grid>
                </Grid>
                
            </div>
        );
    }
   
}

export default LoginPage;